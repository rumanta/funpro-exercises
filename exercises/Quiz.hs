--  Merge sort
merge [] kanan  = kanan
merge kiri []   = kiri
merge kiri@(x:xs) kanan@(y:ys) | x < y = x: merge xs kanan
                               | x >= y = y: merge kiri ys

mergeSort xs = merge (mergeSort kiri) (mergeSort kanan)
  where kiri = take ((length xs) / 2) xs
        kanan = drop ((length xs) / 2) xs

-- Misteri
myConcat = foldl (++) []

aFunction = map sumPair xs ++ concat xss

-- Pythagoras
phytagoras = [(b, a, c) |c <- [1..], a <- [1..c], b <- [1..a], a*a + b*b == c*c]