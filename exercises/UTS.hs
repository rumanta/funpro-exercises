-- Get initials given an input of two strings
initials firstname lastname = [f] ++ ". " ++ [l] ++ "."
  where [f] = firstname
        [l] = lastname

-- Define take function
take' n _
  | n <= 0 = []
take' _ [x] = [x]
take' n (x:xs) = x : take' (n-1) xs

-- zip' :: [a] -> [b] -> [(a, b)]
zip' _ [] = []
zip' [] _ = []
zip' (x:xs) (y:ys) = (x,y):zip' xs ys

-- quickSort :: (Ord a) => [a] -> [a]
quickSort [] = []
quickSort (x:xs) =
  let smallerList = quickSort [a | a <- xs, a <= x]
      biggerList = quickSort [a | a <- xs, a > x]
  in
    smallerList ++ [x] ++ biggerList