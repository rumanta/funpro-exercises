import Data.List

-- Buatlah fungsi divisor yang menerima sebauh bilangan bulat n dan mengembalikan 
-- list bilangan bulat positif yang membagi habis n
divisor n = [x | x <- [1..n], n `mod` x == 0]

-- Buatlah definisi quick sort menggunakan list comprehension
quickSort :: [Int] -> [Int]
quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs, y <= x] ++ [x] ++ quickSort[y | y <- xs, y > x]

-- Buatlah definisi infinite list untuk permutation
perm [] = [[]]
perm ls = [x:xs | x <- ls, xs <- perm(ls \\ [x])]
