module Robot where
  import Array
  import List
  import Monad
  import SOEGraphics
  import Win32Misc (timeGetTime)
  import qualified GraphicsWindows as GW (getEvent)

drawSquare = do penDown
                move
                turnRight
                move
                turnRight
                move
                turnRight
                move

-- cond :: Robot Bool -> Robot a -> Robot a -> Robot a

-- evade :: Robot()
-- evade = cond blocked
--           (do turnRight
--               move)
--           move

-- moveToWall :: Robot()
-- moveToWall = while(isnt blocked)
--               move

spiral :: Robot()
spiral = penDown >> loop 1
  where loop n =
          let twice = do turnRight
                          moven n
                          turnRight
                          moven n
          in cond blocked
              (twice >> turnRight >> moven n)
              (twice >> loop(n + 1))
  
  moven :: Int -> Robot()
  moven n = mapM_ (const move) [1..n]


  moven 3
  = mapM_ (const move) [1..3]

  const move 1 >> const move 2 >> const move 3

  forloop (x:xs) action
    action x >> forloop xs action