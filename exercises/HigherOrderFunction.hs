module Exercise where

import Lib

-- 1 (*). Exercises from chapter 9-10 of The Craft of Functional Programming

-- ** Number 1 **

-- Define the length function using map and sum
toOne x = 1;

myLength xs = sum (map toOne xs)

-- [1,2,3,4,5]
-- [1,1,1,1,1]
-- 5

-- ** Number 2 **

-- What does map (+1) (map (+1) xs) do? Can you conclude anything in
-- general about properties of map f (map g xs), where f and g are arbitrary
-- functions?
arr = [3, 5, 9]
result2 = map (+1) (map (+1) arr)

-- map (+1) ([4, 6, 10])
-- = [5, 7, 10]

-- ** Number 3 **

-- Implement iter function that recursively call a function f for n times.

iter :: (a -> a) -> a -> a
iter n f x = foldl (\s e -> e s) x [f | x <- [1..n]]

-- ** Number 4 **

-- How would you define the sum of the squares of the natural numbers 1
-- to n using map and foldr?

sumSquares n = foldl (+) 0 [1..n]

-- ** Number 5 **

-- How does this function behaves

mystery xs = foldr (++) [] (map sing xs)
  where
    sing x = [x]

-- input = [1,2,3,4,5]
-- Pre process oleh fungsi sing
-- = [[1], [2], [3], [4], [5]]

-- Dilakukan operasi foldr (++)
-- output = [1, 2, 3, 4, 5]

-- Explanation: Fungsi sing akan membuat setiap elemen yg dimap
-- menjadi sebuah list berisikan sebuah integer.
-- Tetapi, fungsi foldr dengan operasi (++), akan menggabungkan list-list berisi angka
-- tersebut menjadi satu list kembali.
-- Kesimpulan: Output akan sama dengan input.